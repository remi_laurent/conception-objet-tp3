class Thing:
	
	def __init__(self,poid=1,name="bidule"):
		self._poid = poid
		self.name=name
		
	def volume(self):
		return self._poid

	def set_name(self,name):
		self.name=name

	def has_name(self,name):
		if self.name==name:
			return True
		else:
			return False


class Box(Thing):
	
	def __init__(self,capacity=None):
		super().__init__(poid=1,name="bidule")
		self._contents = []
		self.is_open=True
		self._capacity = capacity
		self._key=None
		
		
	
	def add(self,truc):
		if self.is_open==True:
			self._contents.append(truc)

	def __contains__(self,truc):
		return truc in self._contents

	def remove(self,truc):
		self._contents.remove(truc)

	def close(self):
		self.is_open=False

	def open(self):
		if self._key == None:
			self.is_open=True

	def action_look(self):
		if self.is_open==True:
			res="la boite contient: "
			res+=", ".join(self._contents)
			return res
		else :
			return "la boite est fermee"
			
	def capacity(self):
		return self._capacity
		
	def set_capacity(self,capacity):
		self._capacity=capacity
		
	def has_room_for(self,chose):
		if self._capacity== None:
			return True
		elif chose <= self._capacity:
			self._capacity=self._capacity-chose
			return True
		else:
			return False

	def action_add(self,chose):
		poid=chose.volume()
		if self.has_room_for(poid) and self.is_open==True:
			self._contents.append(chose)
			return True
		else:
			return False
			
	def find(self,nom):
		if nom in self._contents and self.is_open==True:
			return nom
		else:
			return None 
	
	def set_key(self,t):
		self._key=t

	def open_with(self,t):
		if self._key==t:
			self.is_open=True




class Player(Thing):
	
	def __init__(self):
		super().__init__(poid=70,name="peon")
		self.Boite=super().__init__(self,10)


class Location(Box):
	
	def __init__(self):
		super().__init__(10000)
